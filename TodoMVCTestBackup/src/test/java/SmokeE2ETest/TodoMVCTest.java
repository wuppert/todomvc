package SmokeE2ETest;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.junit.Test;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.CollectionCondition.empty;
import static com.codeborne.selenide.CollectionCondition.exactTexts;
import static com.codeborne.selenide.Condition.cssClass;
import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;

/**
 * TASJ G10o5.
 * Full Coverage with FT.
 * version 12
 * Комментарии по ходу дела для меня. Не цепляйся )
 * From
 * given(Task(ACTIVE, "1"), Task(COMPLETED, "2"));
 * given(ACTIVE, "1");
 * to
 * given(Task.active("1"));
 */

public class TodoMVCTest extends AtTodoMVCPageWithClearedDataAfterEachTest {

    @Test
    public void testTaskFlow() {

//        given(Task(ACTIVE, "1"), Task(COMPLETED, "2"));
//        given(ACTIVE, "1");
//        альтернативные способы ввода
//        given(new Task(ACTIVE, "1"),new Task(COMPLETED, "2"));
//        given(TaskType.ACTIVE, "1", "b");
//        given(Task.active("1"));
        givenActive("1");
        toggleAll();                // complete all
        assertTasksAre("1");

        filterActive();
        assertNoVisibleTasks();
        add("2");
        toggle("2");                //complete
        assertNoVisibleTasks();


        filterCompleted();
        assertTasksAre("1", "2");
        toggle("1");                //reopen
        assertItemsLeft(1);
        assertVisibleTasksAre("2");
        clearCompleted();
        assertNoVisibleTasks();

        filterAll();
        assertTasksAre("1");
    }

    @Test
    public void testEditAtAllFilter() {
        givenActive("2");    //   given(Task.active("2"));      given(ACTIVE,"2");
        edit("2", "2 edited");
        assertTasksAre("2 edited");
    }

    @Test
    public void testStartEditThenCancelAtAllFilter() {
        givenActive("1");   //  given(Task.active("1"));               givenAtActive(ACTIVE, "1");
        startEditThenCancel("1", "1 edited");
        assertTasksAre("1");
    }

    @Test
    public void testDeleteAtCompletedFilter() {
//        givenAtCompleted(COMPLETED,"1", "2");
//        given(new Task(ACTIVE, "1"),new Task(COMPLETED, "2"),new Task(COMPLETED, "3"));
        given(Task.active("1"), Task.completed("2"), Task.completed("3"));
        givenAtComplitedFilter();
        assertItemsLeft(1);
        delete("2");
        assertVisibleTasksAre("3");
        assertItemsLeft(1);
    }

    // Extra Coverage
    @Test
    public void testEditByTABAtAllFilter() {
        givenActive("123");         // given(Task.active("123"));              given(ACTIVE,"123");
        editByTab("123", "123 edited");
        assertTasksAre("123 edited");
    }

    @Test
    public void testDeleteByEmptyingTextAtActiveFilter() {
        givenActive("123");         // given(Task.active("123"));               givenAtActive(ACTIVE,"123");
        filterActive();
        assertTasksAre("123");
        edit("123", " ");
        assertNoTasks();
    }

    @Test
    public void testEditByClickOutsideFieldAtCompletedFilter() {
        givenCompleted("123");      // given(Task.completed("123"));              givenAtCompleted(COMPLETED,"123");
        filterCompleted();
        assertTasksAre("123");
        startEditThenClickOutside("123", "123 edited");
        assertTasksAre("123 edited");

    }

    ElementsCollection tasks = $$("#todo-list>li");

    @Step
    private void add(String... taskTexts) {
        for (String text : taskTexts) {
            $("#new-todo").setValue(text).pressEnter();
        }
    }

    @Step
    private SelenideElement startEdit(String fromTaskText, String toTaskText) {
        tasks.find(exactText(fromTaskText)).doubleClick();
        return tasks.findBy(cssClass("editing")).find(".edit").setValue(toTaskText);
    }

    @Step
    private void edit(String fromTaskText, String toTaskText) {
        startEdit(fromTaskText, toTaskText).pressEnter();
    }

    @Step
    private void startEditThenCancel(String fromTaskText, String toTaskText) {
        startEdit(fromTaskText, toTaskText).pressEscape();
    }

    @Step
    private void editByTab(String fromTaskText, String toTaskText) {
        startEdit(fromTaskText, toTaskText).pressTab();
    }

    @Step
    private void startEditThenClickOutside(String fromTaskText, String toTaskText) {
        startEdit(fromTaskText, toTaskText);
        $("body").click();
    }

    @Step
    private void filterAll() {
        $(By.linkText("All")).click();
    }

    @Step
    private void filterActive() {
        $(By.linkText("Active")).click();
    }

    @Step
    private void filterCompleted() {
        $(By.linkText("Completed")).click();
    }

    @Step
    private void assertFilterActive() {
        $("#filters>li>.selected").shouldHave(exactText("Active"));
    }

    @Step
    private void assertTasksAre(String... taskTexts) {
        tasks.shouldHave(exactTexts(taskTexts));
    }

    @Step
    private void assertVisibleTasksAre(String... taskTexts) {
        tasks.filter(visible).shouldHave(exactTexts(taskTexts));
    }

    @Step
    private void delete(String taskText) {
        tasks.find(exactText(taskText)).hover().$(".destroy").click();
    }

    @Step
    private void toggle(String taskText) {
        tasks.find(exactText(taskText)).$(".toggle").click();
    }

    @Step
    private void toggleAll() {
        $("#toggle-all").click();
    }

    @Step
    private void clearCompleted() {
        $("#clear-completed").click();
    }

    @Step
    private void assertNoTasks() {
        tasks.shouldBe(empty);
    }

    @Step
    private void assertNoVisibleTasks() {
        tasks.filter(visible).shouldBe(empty);
    }

    @Step
    private void assertItemsLeft(int activeTasksCount) {
        $("#todo-count strong").shouldHave(exactText(String.valueOf(activeTasksCount)));
    }

    public static class Task {
        String taskText;
        boolean taskType;

        public Task(boolean taskType, String taskText) {
            this.taskText = taskText;
            this.taskType = taskType;
        }

        public String toString() {
            return "{\"completed\":" + taskType + ",\"title\":\"" + taskText + "\"}";
        }

        public static Task completed(String text) {
            return new Task(true, text);
        }

        public static Task active(String text) {
            return new Task(false, text);
        }
    }

    @Step
    public String prepareGivenCommand(Task... tasks) {

        StringBuilder results = new StringBuilder("localStorage.setItem('todos-troopjs', '[");

        for (Task task : tasks) {
            results.append(task).append(",");
        }

        if (tasks.length > 0) {
            results.setLength(results.length() - 2);
        }
//        results.append("}]')");
//        return String.valueOf(results);
//        кажется такой вариант тебе понравится больше
        return results.append("}]')").toString();
    }


    @Step
    public void given(Task... tasks) {
        String jsCommand = prepareGivenCommand(tasks);
        executeJavaScript(jsCommand);
        refresh();
    }

    @Step
    public void given(boolean taskType, String... taskTexts) {
        Task[] tasks = new Task[taskTexts.length];

        for (int i = 0; i < taskTexts.length; i++) {
            tasks[i] = new Task(taskType, taskTexts[i]);
        }
        given(tasks);
    }

    public void givenActive(String... taskTexts) {
        Task[] tasks = new Task[taskTexts.length];
        for (int i = 0; i < taskTexts.length; i++) {
            tasks[i] = new Task(false, taskTexts[i]);
        }
        given(tasks);
    }

    public void givenCompleted(String... taskTexts) {
        Task[] tasks = new Task[taskTexts.length];
        for (int i = 0; i < taskTexts.length; i++) {
            tasks[i] = new Task(true, taskTexts[i]);
        }
        given(tasks);
    }


    @Step
    public void givenAtActive(Task... tasks) {
        given(tasks);
        givenAtActiveFilter();
    }

    @Step
    public void givenAtCompleted(Task... tasks) {
        given(tasks);
        givenAtComplitedFilter();
    }

    @Step
    public void givenAtActive(boolean taskType, String... taskTexts) {
        given(taskType, taskTexts);
        givenAtActiveFilter();
    }


    @Step
    public void givenAtCompleted(boolean taskType, String... taskTexts) {
        given(taskType, taskTexts);
        givenAtComplitedFilter();
    }


    public void givenAtAllFilter() {
        open("https://todomvc4tasj.herokuapp.com/#");
    }

    public void givenAtActiveFilter() {
        open("https://todomvc4tasj.herokuapp.com/#/active");
    }

    public void givenAtComplitedFilter() {
        open("https://todomvc4tasj.herokuapp.com/#/completed");
    }
}

